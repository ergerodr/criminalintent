package com.bignerdranch.android.criminalintent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bignerdranch.android.criminalintent.database.CrimeBaseHelper;
import com.bignerdranch.android.criminalintent.database.CrimeCursorWrapper;
import com.bignerdranch.android.criminalintent.database.CrimeDbSchema.CrimeTable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ergerodr on 2/6/16.
 */
public class CrimeLab {
    private static CrimeLab sCrimeLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static CrimeLab get(Context context){
        if (sCrimeLab == null){
            sCrimeLab = new CrimeLab(context);
        }
        return sCrimeLab; //if instance already exists, return that instance.
    }

    private CrimeLab(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new CrimeBaseHelper(mContext).getWritableDatabase();
    }

    public void addCrime(Crime c){
        ContentValues values = getContentValues(c); //add row in database, with col fields filled
        mDatabase.insert(CrimeTable.NAME, null, values);
    }

    public List<Crime> getCrimes(){
        List<Crime> crimes = new ArrayList<>();

        CrimeCursorWrapper cursor = queryCrimes(null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                crimes.add(cursor.getCrime());
                cursor.moveToNext();
            }
        }finally {
            cursor.close();
        }
        return crimes;
    }

    public Crime getCrime(UUID id){//read from database.
        CrimeCursorWrapper cursor = queryCrimes(
                CrimeTable.Cols.UUID + " = ?",
                new String[] { id.toString() }
        );
        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            return cursor.getCrime();
        } finally{
            cursor.close();
        }
    }

    public void updateCrime(Crime crime){//update rows in database
        String uuidString = crime.getId().toString();
        ContentValues values = getContentValues(crime);
        mDatabase.update(CrimeTable.NAME, values,
                CrimeTable.Cols.UUID + " = ?",
                new String[] { uuidString });
/*Now we have to specify WHICH rows get updated. We do that by building a "where" clause (IE. 3rd arg)
* and then specifying values for the arguments in the where clause (the final String[] array)*/
    }

    private static ContentValues getContentValues(Crime crime){//Fill in column fields
        ContentValues values = new ContentValues();//like "bundle", (key, value) storage
        values.put(CrimeTable.Cols.UUID, crime.getId().toString());
        values.put(CrimeTable.Cols.TITLE, crime.getTitle());
        values.put(CrimeTable.Cols.DATE, crime.getDate().getTime());
        values.put(CrimeTable.Cols.SOLVED, crime.isSolved());
        return values;
/*  Here we use the column names as our keys. The keys specify the columns that we want to
* insert or update in the database.  "_id" is the only column not specified because it is
* automatically created for us as a unique row ID. */
    }

    private CrimeCursorWrapper queryCrimes(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                CrimeTable.NAME, //Query "this" table
                null,   //null selects all columns
                whereClause,
                whereArgs,
                null,   //groupBy
                null,   //having
                null    //orderBy
        );
        return new CrimeCursorWrapper(cursor);
    }
}

/*  When you call getWritableDatabase() here, CrimeBaseHelper well do the following...,
    1) Open up a /data/data/com.bignerdranch.android.criminalintent/databases/crimeBase.db, creating
a new database file if it does not already exist.
    2) If this is the first time the database has been created, call onCreate(SQLiteDatabase), then
save out the latest version number.
    3) If this is not the first time, check the version number in the database. If the version
number in CrimeBaseHelper is higher, call onUpgrade(SQLiteDatabase, int, int)

    PUT YOUR CODE TO CREATE THE INITIAL DATABSE IN "onCreate(SQLiteDatabase)", and your code to
         handle any upgrades in onUpgrade(SQLiteDatabase, int, int).

 */
