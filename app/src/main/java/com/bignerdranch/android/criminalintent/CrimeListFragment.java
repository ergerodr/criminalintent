package com.bignerdranch.android.criminalintent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
//The LinearLayoutManager handles the positioning of items and also defines the scrolling
//behavior. If the LayoutManager is not there, RecyclerView will crash.
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by ergerodr on 2/6/16.
 */
public class CrimeListFragment extends Fragment {
    private static final String SAVED_SUBTITLE_VISIBLE = "subtitle";

    private RecyclerView mCrimeRecyclerView;
    private CrimeAdapter mAdapter;
    private boolean mSubtitleVisible;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_crime_list, container, false);
        mCrimeRecyclerView = (RecyclerView) view.findViewById(R.id.crime_recycler_view);
        //RecyclerView requires a LayoutManager to work. If you forget to give it one, it crashes.
        mCrimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (savedInstanceState != null){
            mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }
        updateUI();

        return view;
    }

    @Override
    public void onResume(){
        //"onResume()" is the safest place to take action to
        // update a fragment's view
        super.onResume();
        updateUI();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }

    @Override//This method creates our toolbar menu
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        //Menus are managed by callbacks from the activity class.
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime_list, menu);
/*The Fragment Manager is responsible for calling this method. When the activity receives its
"onCreateOptionsMenu(...)" callback from the OS. You must explicitly tell the "FragmentManager"
that your fragment should receive a call to "onCreateOptionsMenu" by using "setHasOptionsMenu()"
*/
        MenuItem subtitleItem = menu.findItem(R.id.menu_item_show_subtitle);
        if (mSubtitleVisible){
            subtitleItem.setTitle(R.string.hide_subtitle);
        }else{
            subtitleItem.setTitle(R.string.show_subtitle);
        }
    }

    @Override//method called when user presses menu item
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_item_new_crime://fragment_crime_list.xml
                Crime crime = new Crime();
                CrimeLab.get(getActivity()).addCrime(crime);
                Intent intent = CrimePagerActivity.newIntent(getActivity(), crime.getId());
                startActivity(intent);//to start editing the new crime launch "CrimePagerActivity"
                return true;
            case R.id.menu_item_show_subtitle:
                mSubtitleVisible = !mSubtitleVisible;
                getActivity().invalidateOptionsMenu();;
                updateSubtitle();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSubtitle(){
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        int crimeCount = crimeLab.getCrimes().size();
        String subtitle = getString(R.string.subtitle_format, crimeCount);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if(!mSubtitleVisible){
            subtitle = null;
        }
        //For legacy reasons, the toolbar is still referred to as "action bar" in many places
        // within the (AppCompat) library.
        activity.getSupportActionBar().setSubtitle(subtitle);
    }

    //updateUI connects our Adapter to our RecyclerView using getAdapter.
    private void updateUI(){
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();
        if(mAdapter == null) {
            mAdapter = new CrimeAdapter(crimes);
            mCrimeRecyclerView.setAdapter(mAdapter);
        } else{
            mAdapter.setCrimes(crimes);
            mAdapter.notifyDataSetChanged();
        }
        updateSubtitle();
    }

    private class CrimeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    //This "ViewHolder" definition maintains references to the following.
        private Crime mCrime;
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private CheckBox mSolvedCheckBox;

        public CrimeHolder(View itemView){
            //"itemView" needs to be a TextView or else it will crash.
            super(itemView);
            //we pass in 'this' RecyclerView's onClickListener as it "implements" an onClickListener.
            itemView.setOnClickListener(this);
            mTitleTextView = (TextView)
                    itemView.findViewById(R.id.list_item_crime_title_text_view);
            mDateTextView = (TextView)
                    itemView.findViewById(R.id.list_item_crime_date_text_view);
            mSolvedCheckBox = (CheckBox)
                    itemView.findViewById(R.id.list_item_crime_solved_check_box);
        }

        public void bindCrime(Crime crime){
            mCrime = crime;
            mTitleTextView.setText(mCrime.getTitle());
            mDateTextView.setText(mCrime.getDate().toString());
            mSolvedCheckBox.setChecked(mCrime.isSolved());
        }

        @Override
        public void onClick(View v){
            Intent intent = CrimePagerActivity.newIntent(getActivity(), mCrime.getId());
            startActivity(intent);
        }

    }

    private class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> {

        private List<Crime> mCrimes;

        public CrimeAdapter(List<Crime> crimes){
            mCrimes = crimes;
        }

        @Override
        public CrimeHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            //create view, Custom layout instead of standard "android.R.layout.simple_list_item_1"
            View view = layoutInflater.inflate(R.layout.list_item_crime, parent, false);
            //wrap it in a "ViewHolder"
            return new CrimeHolder(view);
        }

        @Override
        public void onBindViewHolder(CrimeHolder holder, int position){
            //get crime from ArrayList (CrimeLab.java)
            Crime crime = mCrimes.get(position);
            //To bind a View, use "position" to find the right model data.
            //Then update the View to reflect that model data. (setText)

            //Send title to ViewHolder's (in this case "CrimeHolder's") TextView
            //holder.mTitleTextView.setText(crime.getTitle());

            holder.bindCrime(crime);
        }

        @Override
        public int getItemCount(){
            return mCrimes.size();
        }

        public void setCrimes(List<Crime> crimes){
            mCrimes = crimes;
        }

    }


}

/*
VIEWHOLDER; holds on to a View. Below is a typical ViewHolder Subclass.

public class ListRow extends RecyclerView.ViewHolder {
    public ImageView mThumbnail;
    public ListRow(View view) {
        super(view); //1)
        mThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
    }
}

1) "itemView" is a field which your superclass "RecyclerView.ViewHolder" assigns for you.
The itemView field is your ViewHolder's reason for existing: -> it holds a reference to the
entire "View" you passed in to "super(view)"

TYPICAL USAGE OF A "ViewHolder"

ListRow row = new ListRow(inflater.inflate(R.layout.list_row, parent, false));
View view = row.itemView;
ImageView thumbnailView = row.mThumbnail;

    A "RecyclerView" Never creates Views by themselves. It always creates "ViewHolders" which
bring their itemViews along for the ride.

BUT, a "RecyclerView" does not create "ViewHolders" itself. Instead, it asks an "adapter".

ADAPTER: a controller object that sits between the "RecyclerView" and the data set that the
RecyclerView should display.

The adapter is responsible for...,
    1) Creating the necessary "ViewHolders"
    2) Binding ViewHolders to data from the model layer.

    To build an adapter, you first define a subclass of RecyclerView.Adapter. This subclass will
wrap the list of crimes you get from CrimeLab.

A TYPICAL RECYCLERVIEW-ADAPTER CONVERSATION
    1) R-View asks how many objects are in the list by calling the adapter's getItemCount() method
    2) R-View calls the adapter's createViewHolder() method which returns a View to display.
    3) R-View calls onBindViewHolder() with a Viewholder as param1 and its position as param2.
The adapter will look up the model data for that position and bind it to the ViewHolder's View.
    4) R-View will place a list item on the screen.
NOTE: once a sufficient number of ViewHolders have been created, R-View stops calling
createViewHolder() and instead recycles views using onBindViewHolder()

*/
