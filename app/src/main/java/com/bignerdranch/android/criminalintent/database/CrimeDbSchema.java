package com.bignerdranch.android.criminalintent.database;

/**
 * Created by ergerodr on 2/22/16.
 */
public class CrimeDbSchema {
//With this schema we will be able to refer to the column named "title" using...
    //CrimeTable.Cols.TITLE
    public static final class CrimeTable{
        public static final String NAME = "crimes";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String SOLVED = "solved";
        }
    }
}
/*
METHODS TO OPEN A DATABASE FILE INTO AN INSTANCE OF "SQLiteDatabase"
    1)openOrCreateDatabase(...)
    2)databaseList()

IN PRACTICE WE WILL ALWAYS NEED TO FOLLOW A FEW BASIC STEPS.
    1. Check to see if the database already exists.
    2. If it does not, create it and create the tables and initial data it needs.
    3. If it does, open it up and see what version of your CrimeDbSchema it has. (You may want to add or
remove things in future versions of CriminalIntent .)
    4. If it is an old version, run code to upgrade it to a newer version.
SQLiteOpenHelper handles all of this for us. (See CrimeBaseHelper.java)

 */