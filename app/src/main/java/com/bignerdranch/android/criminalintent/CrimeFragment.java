package com.bignerdranch.android.criminalintent;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup; //<-- a special view that contains other views
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Date;
import java.util.UUID;

/**
 * Created by ergerodr on 1/22/16.
 *
 * This class is a controller that interacts with model and view objects.
 * It's job is to present the details of a specific crime and update those
 * details as the user changes them.
 */
public class CrimeFragment extends Fragment {

    private static final String ARG_CRIME_ID = "crime_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final int REQUEST_DATE = 0;

    private Crime mCrime;
    private EditText mTitleField;
    private Button mDateButton;
    private CheckBox mSolvedCheckBox;

    public static CrimeFragment newInstance(UUID crimeId){
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME_ID, crimeId);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);//attach Bundle to the CrimeFragment
        return fragment;
    }

    /* Fragment.onCreate() is a public method.
           -Must be public because they will be called by whatever activity
            is hosting the fragment.
       Activity.onCreate() is a protected method.
     */
    @Override
    public void onCreate(Bundle savedInstanceState){
        /*fragment instances are configured here, but these instances and
their views are created in "onCreateView". "onCreateView" is where you inflate
the layout for the fragment's view and return the inflated View to the hosting activity.
        */
        super.onCreate(savedInstanceState);
        UUID crimeId = (UUID) getArguments().getSerializable(ARG_CRIME_ID);
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);
    }

    @Override
    public void onPause(){
        super.onPause();
        CrimeLab.get(getActivity())
                .updateCrime(mCrime);
/*Crime instances get modified in CrimeFragment, and will need to be written out when CrimeFragment
* is done. This update CrimeLab's copy of the crime. */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        /* 2nd parameter in "LayoutInflater.inflate()" is the view's parent, The third
parameter is set to false so we can add the view in the activity's code.
         */
        View v = inflater.inflate(R.layout.fragment_crime, container, false);

        mTitleField = (EditText) v.findViewById(R.id.crime_title);
        mTitleField.setText(mCrime.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCrime.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDateButton = (Button) v.findViewById(R.id.crime_date);
        updateDate();
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                FragmentManager manager = getActivity()
                        .getSupportFragmentManager();
                DatePickerFragment dialog = DatePickerFragment
                        .newInstance(mCrime.getDate());
                //Needed to return data (I.E., the date) back to CrimeFragment.
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        mSolvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setChecked(mCrime.isSolved());
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        if(requestCode == REQUEST_DATE){
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mCrime.setDate(date);
            updateDate();
        }
    }
    private void updateDate(){
        mDateButton.setText(mCrime.getDate().toString());
    }
}
    /*
Attaching arguments to a fragment

    1) To attach the arguments bundle to a fragment, you call Fragment.setArguments(Bundle).
    2) Attaching arguments to a fragment must be done after the fragment is created
but before it is added to an activity.

    To hit this window, Android programmers follow a convention of adding a static method named
"newInstance()" to the Fragment class. This method creates the fragment instance, bundles up and
sets its arguments.

    When the hosting activity needs an instance of that fragment, you have it call "newInstance()"
rather than calling the constructor directly. The activity can pass in any required parameters to
"newInstance(...)" that the fragment needs to create its arguments.
*/
