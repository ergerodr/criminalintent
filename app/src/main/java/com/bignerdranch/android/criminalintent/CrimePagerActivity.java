package com.bignerdranch.android.criminalintent;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

/**
 * Created by ergerodr on 2/21/16.
 */
public class CrimePagerActivity extends AppCompatActivity{
    private static final String EXTRA_CRIME_ID =
            "com.bignerdranch.android.criminalintent.crime_id";

    private ViewPager mViewPager;
    private List<Crime> mCrimes;

    public static Intent newIntent(Context packageContext, UUID crimeId){
        Intent intent = new Intent(packageContext, CrimePagerActivity.class);
        intent.putExtra(EXTRA_CRIME_ID, crimeId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crime_pager);

        UUID crimeId = (UUID) getIntent().getSerializableExtra(EXTRA_CRIME_ID);

        mViewPager = (ViewPager) findViewById(R.id.activity_crime_pager_view_pager);

        mCrimes = CrimeLab.get(this).getCrimes();
        FragmentManager fragmentManager = getSupportFragmentManager();

        //below we set the adapter to be an unnamed instance of "FragmentStatePagerAdapter"
        //which needs a "FragmentManager" to be able to add fragments that "getItem(Int)" returns.
        //
        //Layout <--> FragmentManager <--> FragmentStatePagerAdapter
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Crime crime = mCrimes.get(position);
                return CrimeFragment.newInstance(crime.getId());
            }

            @Override
            public int getCount() {
                return mCrimes.size();
            }
        });

        //When you find the index of the crime to display by looping through and checking
        //each crime's ID. When you find the "Crime" instance whose "mId" matches the "crimeId"
        //in the intent extra, set the current item to the index of that Crime.
        for(int i = 0; i < mCrimes.size(); i++){
            if(mCrimes.get(i).getId().equals(crimeId)){
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}

/*
    QUESTION: When would you need to implement the "PagerAdapter" yourself instead of using
"FragmentStatePagerAdapter" which hides a lot of behind the scenes code?
    ANSWER: When you want a ViewPager to host something other than fragments. If you want to
host normal "View" objects in a "ViewPager", like a few images, you implement the raw
PagerAdapter interface.

    ViewPager and PagerAdapter IS MORE COMPLICATED THAN RecyclerView and Adapter

    Instead of an onBindViewHolder(...) method that returns a view holder and its corresponding view,
PagerAdapter has the following methods:

1)public Object instantiateItem(ViewGroup container, int position)
2)public void destroyItem(ViewGroup container, int position, Object object)
3)public abstract boolean isViewFromObject(View view, Object object)

    1) tells the pager adapter to create an item view AT THE PagerAdapter's DISCRETION
for a given position and add it to a container ViewGroup.

    2) destroys the item created in 1)

    3) ViewPager uses 3) to figure out which item's View it is once the View has been created.
The "Object" parameter is an object received from a call to 1)

    EXAMPLE: if ViewPager calls instantiateItem(ViewGroup, 5) and receives object A,
isViewFromObject(View, A) should return true if the View passed in is for item 5,
and false otherwise

 */
